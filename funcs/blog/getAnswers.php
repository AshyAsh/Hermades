<?php

function Blog_getAnswers($conn, $pId, $limit) {
  $answers = [];

  $stmt = $conn->prepare("SELECT content,date FROM blog WHERE isAnswer=1 AND
    answerTo=? LIMIT ?");
  $stmt->bind_param("ii", $pId, $limit);
  $stmt->execute();
  $stmt->bind_result($content, $date);
  while($stmt->fetch()) {
    $answers[] = $content;
    $answers[] = $date;
  }
  $stmt->close();

  return $answers;
}

?>
