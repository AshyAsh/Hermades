<?php

function Forum_addPost($conn, $community, $content, $date, $isAnswer, $ansId) {
        $success = 0;

        if (!empty($content)) {
                $content = htmlspecialchars($content);

                $stmt = $conn->prepare("INSERT INTO forums_posts(content,
                  community, isAnswer, answeredPost, date) VALUES(?,?,?,?,?)");
                $stmt->bind_param("ssiis", $content, $community, $isAnswer,
                  $ansId, $date);
                if ($stmt->execute()) {
                        $stmt->close();
                        $success = 1;
                } else {
                        $stmt->close();
                        $success = 0;
                }

                if ($success) {
                        $stmt = $conn->prepare("SELECT posts FROM
                                forums_communities WHERE slug=?");
                        $stmt->bind_param("s", $community);
                        $stmt->execute();
                        $stmt->store_result();
                        $stmt->bind_result($posts);
                        $stmt->fetch();
                        $stmt->close();

                        $posts++;

                        $stmt = $conn->prepare("UPDATE forums_communities SET
                                posts=? WHERE slug=?");
                        $stmt->bind_param("is", $posts, $community);
                        if (!$stmt->execute()) {
                                echo "<script>alert('" . $conn->error . "')</script>";
                        }
                        $stmt->close();
                }
        }

        return $success;
}

?>
