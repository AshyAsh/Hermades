<?php

require "../inc/db.php";
include "../inc/head.php";
include "../inc/libs/Parsedown.php";

include "../funcs/blog/getPost.php";

if (empty($_GET["id"])) {
  die("Error: malformed URL");
}

$id = $_GET["id"];

if (isset($_POST["ans"])) {
  include "../funcs/blog/postAnswer.php";

  if (Blog_postAnswer($conn, $_POST["content"], $id)) {
    echo "Asnwer posted successfully";
  }
}

$post = Blog_getPost($conn, $id);

$parsedown = new Parsedown();

?>
<hr>
<p><?php echo $parsedown->text($post[1]); ?></p>
<p><b>Published on:</b> <?php echo $post[2]; ?></p>

<form class="answer" style="border: 1px solid #000; width: 100%; padding: 5px;"
method="POST">
  <textarea rows="1" cols="80" name="content"></textarea><br>
  <input type="submit" name="ans" value="Answer">
  <input type="hidden" name="id" value="<?php echo $posts[$i]; ?>">
</form>

<?php

include "../funcs/blog/getAnswers.php";

$answers = Blog_getAnswers($conn, $id, 100);

for ($i = 0; $i < sizeof($answers); $i+=2) {
?>
<hr>
<div class="answer" style="margin-left: 40px;">
  <p><?php echo $parsedown->text($answers[$i]); ?></p>
  <p><b>Published on</b>: <?php echo $answers[$i+1]; ?></p>
</div>
<?php } ?>
