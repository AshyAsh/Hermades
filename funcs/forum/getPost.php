<?php

function Forum_getPost($conn, $id) {
  $stmt = $conn->prepare("SELECT content,date,community FROM forums_posts WHERE
id=? LIMIT 1");
  $stmt->bind_param("i", $id);
  $stmt->execute();
  $stmt->bind_result($content, $date, $community);
  $stmt->fetch();
  $stmt->close();

  return [$content, $date, $community];
}

?>
