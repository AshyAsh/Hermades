<?php

include "inc/head.php";

?>
<hr>
<h1>Philosophy</h1>
<h3>About Free Software</h3>
<p>“Free software” means software that respects users' freedom and community.
Roughly, it means that <b>the users have the freedom to run, copy, distribute,
study, change and improve the software</b>.  Thus, “free software” is a matter of
liberty, not price. To understand the concept, you should think of “free” as in
“free speech,” not as in “free beer”. We sometimes call it “libre software,”
borrowing the French or Spanish word for “free” as in freedom, to show we do not
mean the software is gratis. </p>
<p>We campaign for these freedoms because everyone deserves them. With these
freedoms, the users (both individually and collectively) control the program and
what it does for them. When users don't control the program, we call it a
“nonfree” or “proprietary” program. The nonfree program controls the users, and
the developer controls the program; this makes the program <a
href="https://www.gnu.org/philosophy/free-software-even-more-important.html">an
instrument of unjust power. </a></p>
<h4>The four essential freedoms</h4>
<p>A program is free software if the program's users have the four essential
freedoms:</p>
<ul>
  <li>The freedom to run the program as you wish, for any purpose (freedom
0).</li>
  <li>The freedom to study how the program works, and change it so it does your
computing as you wish (freedom 1). Access to the source code is a precondition
for this. </li>
  <li>The freedom to redistribute copies so you can help others (freedom 2).
</li>
  <li>The freedom to distribute copies of your modified versions to others
(freedom 3). By doing this you can give the whole community a chance to benefit
from your changes. Access to the source code is a precondition for this. </li>
</ul>

<a href="https://www.gnu.org/philosophy/free-sw.en.html">More info here</a>

<p>In order to grant you all these freedoms, all the content of this website is
licensed under the following licenses:</p>

<ul>
  <li>Site source code: <a href="https://www.gnu.org/licenses/gpl-3.0.html">GPL
  3.0 or above</a></li>
  <li>Content of this site (media, courses, blog and even things that are posted
  on the forums): <a href="https://creativecommons.org/licenses/by-nd/3.0/">CC
  BY-ND 3.0</a></li>
</ul>
