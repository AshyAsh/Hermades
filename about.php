<?php

include "inc/head.php";

?>

<h1>Hermades Lifestyle</h1>
<p>Note: This is based in the way stallman did it on his <a
href="https://stallman.org">page</a>.</p>

<h2>Music</h2>
<ul>
  <li>Music genres I like are jazz, swing, easy listening, blues, industrial
metal, spanish rap, folk, country and tango. Singers/groups I like a lot are
Dean Martin, Frank Sinatra, Elvis Presley, Ella Fitzgerald, Louis Armstrong,
Bobby Darin, Nat King, Giulia y Los Tellarini, Bing Crosby, Bob Dylan, Hank
Williams, Johnny Cash, The Irish Rovers, Manuel Medrano, Marilyn Manson, Rob
Zombie, Slipknot, Peggy Lee, Canserbero, El Chojin, Zpu, Billie Holiday, Sara
Vaughan, Nina Simone, Angus and Julia Stone, Liana Malva, JUNG and several
others I can't remember right now.</li>
  <li>I know the lyrics of a big part of the songs I usually listen to, because
if I don't I can't focus, I need to sing while listening to the music
(ironically, that helps me to get focused).</li>
  <li>I do almost everything while listening to the music, otherwise I will get
bored so fast (while writing this, I am listening to the music).</li>
  <li>I love both slow and fast music but I don't listen to them
interchangeably, when I want to to do things fast, I listen to fast/heavy music,
that helps me so much.</li>
</ul>

<h2>Books</h2>
<ul>
  <li>I am almost all the time reading a book, but I am not good at recommending
books.</li>
  <li>Genres I like the most are romance, mistery, horror, programming,
philosophy, psychology and security-related books.</li>
</ul>

<h2>Food</h2>
<ul>
  <li>I am an omnivore.</li>
  <li>I don't like alcohol, for me it tastes horrible.</li>
  <li>I don't hate any food or ingredient.</li>
  <li>I love food but I don't like eating, I think it is a waste of time.</li>
</ul>

<h2>Religion</h2>
<ul>
  <li>I am an atheist, for scientific reasons.</li>
  <li>I hate when people stop doing things because they don't want to upset
their gods (I mean good things, for example, you can't kill or steal
independently if you have a god or not)</li>
  <li>There's more wisdom on doubt than infinite knowledge.</li>
</ul>

<h2>Clothing</h2>
<ul>
  <li>I don't care that much about the things I wear on, I simply take the first
clothes I find (if they are clean) and I put them on.</li>
</ul>

<h2>Surveillance issues</h2>
<ul>
  <li>See also <a href="/about/computing">How I do my computing</a></li>
  <li>I refuse to carry a phone with me all the time, I hate phones and
surveillance is one of the resons for me to think that (even though sometimes I
am forced to carry a phone and I hate it).</li>
</ul>

<h2>Holidays</h2>
<ul>
  <li>I don't like any holiday. I don't like christmas or those new year
celebrations</li>
</ul>

<h2>Languages</h2>
<ul>
  <li>I speak three languages: Spanish, English and Italian.</li>
  <li>The way I learn languages is:</li>
  <ul>
    <li>First, I learn words in that language and try to form simple
sentences.</li>
    <li>Then, I learn some grammar rules and try to apply them in the sentences
I wrote beforehand.</li>
    <li>When I can understand a bit of that language I will start reading books
for children, each time I see an unknown word I will take a dictionary, search
for its definition and try to write down five sentences using it.</li>
    <li>Once I feel comfortable and confident with that language I will start
watching videos, documentaries and listening to podcasts.</li>
    <li>Finally, I will start looking for natives to practise my abilities in
that language.</li>
  </ul>
</ul>

<h2>Facts about me</h2>
<ul>
  <li>I never give "sensitive" information about me to no one. Some people think
my name is Ash, some others think it's Sophie, some others think it's Henry,
some people think I am from Argentina, some others think I am from Mexico and
none of them are real.</li>
</ul>
