<?php

function Forum_getPosts($conn, $community, $page, $maxPerPage) {
        $posts = [];

        $stmt = $conn->prepare("SELECT * FROM forums_posts WHERE community=?
                AND isAnswer=0 ORDER BY id DESC LIMIT ?,?");
        $stmt->bind_param("sii", $community, $page, $maxPerPage);
        $stmt->execute();
        $stmt->bind_result($id, $content, $community, $isAnswer, $answeredPost,
                $date);
        while ($stmt->fetch()) {
                $posts[] = $id;
                $posts[] = $content;
                $posts[] = $community;
                $posts[] = $isAnswer;
                $posts[] = $answeredPost;
                $posts[] = $date;
        }
        $stmt->close();

        return $posts;
}

?>
