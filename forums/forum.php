<?php

require "../inc/db.php";
include "../inc/head.php";
include "../inc/libs/Parsedown.php";

$numPerPage = 20;
$page = 1;

if (isset($_GET["page"])) {
  $page = $_GET["page"];
}

$community = $_GET["community"];

if (empty($community)) {
        die("<br>The entered community is not valid");
}

$parsedown = new Parsedown();

$stmt = $conn->prepare("SELECT name FROM forums_communities WHERE slug=?");
$stmt->bind_param("s", $community);
$stmt->execute();
$stmt->store_result();
if ($stmt->num_rows < 1) {
        $stmt->close();
        die("<br>The entered community doesn't exist");
}
$stmt->bind_result($communityName);
$stmt->fetch();
$stmt->close();

if (isset($_POST["post"])) {
        include "../funcs/forum/addPost.php";

        date_default_timezone_set("UTC");

        $content = $_POST["postContent"];
        $date = date("d/m/Y H:i") . " UTC";

        $isAnswer = 0;
        $answerId = -1;

        if (isset($_POST["answerId"])) {
          $isAnswer = 1;
          $answerId = $_POST["answerId"];
        }

        if (Forum_addPost($conn, $community, $content, $date, $isAnswer,
          $answerId)) {
                echo "<br>Post added successfully";
                header("Reload: 0");
        }
}

?>

<form method="POST">
        <p>Post</p>
        <textarea name="postContent" cols="80" rows="5"></textarea>
        <br><input type="submit" name="post" value="Post">
</form>

<p>Posts:</p>
<hr>

<?php

include "../funcs/forum/getPosts.php";
include "../funcs/forum/getAnswers.php";

$startFrom = ($page-1) * $numPerPage;

$posts = Forum_getPosts($conn, $community, $startFrom, $numPerPage);

for ($i = 0; $i < sizeof($posts); $i += 6) {
?>

<div class="post" border="1">
        <p><?php echo $parsedown->text($posts[$i+1]); ?></p>
        <p><b>Published on</b>: <?php echo $posts[$i+5]; ?></p>
        <a href="/forum/post/<?php echo $posts[$i]; ?>">Full screen</a>

        <div class="answers" style="border: 1px solid #f0a; margin-left: 20px;">
          <?php
            $answers = Forum_getAnswers($conn, $posts[$i], 4);
            for ($j = 0; $j < sizeof($answers); $j+=2) {
          ?>
            <p><?php echo $parsedown->text($answers[$j]); ?></p>
            <p><b>Published on</b>: <?php echo $answers[$j+1]; ?></p>
          <?php } ?>

          <div class="answer" style="padding: 5px;">
            <form method="POST">
              <textarea cols="80" rows="1" placeholder="Answer to this post"
  name="postContent"></textarea>
              <input type="submit" value="Answer" name="post">
              <input type="hidden" value="<?php echo $posts[$i]; ?>"
  name="answerId">
            </form>
          </div>

        </div>
</div>

<?php
}

include "../funcs/forum/getPages.php";

$pages = Forum_getPages($conn, $community, $numPerPage);
for ($j = 0; $j < $pages; $j++) {
  $page = $j+1;
  echo "<a href=\"?page=" . $page ."\">" . $page . "</a> ";
}

?>
