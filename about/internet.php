<?php

include "../inc/head.php";

?>

<h1>How I do my computing?</h1>
<p>Note: This is based the way stallman did it on his <a
href="https://stallman.org/stallman-computing.html">page</a>.</p>

<h2>My computer</h2>
<p>I use an Asus X441N, which runs a free operating system (Trisquel
GNU/Linux).</p>

<h2>GNU/Linux distro</h2>

<p>I do not have a preferred GNU/Linux distro. I recommend all the <a
href="https://www.gnu.org/distros/">ethical distros</a>.</p>
<p>I've been using Debian-based and Arch-based distros for a long time, but none
of them are my favourite I usually switch between them (Trisquel and
Hyperbola).</p>

<h2>What I do on my computer</h2>
<p>I am almost all the time working in any project, reading or watching videos.
The projects I am currently working on are in <a href="/projects">this page</a>,
I have a lot of books downloaded on my computer, such as the emacs and gdb
manual, some others about programming (assembly and C) and also some hacking
books.</p>
<p>When I am not working on any project, reading or watching videos I am usually
hacking my computer, modifying things, opening files, search about the ones I
don't know, create little programs in programming languages I am learning and
those kind of stuff.</p>

<h2>How I use my computer</h2>
<p>See also: <a href="/about/software">Software that I use</a></p>
<p>I don't like using a mouse, that's the reason of my love for tiling
window managers. I am currently using dwm, I also use ranger as file
manager, emacs as text editor and lynx for several purposes (see: How
I use the internet for more info).</p>
<p>I don't like wasting time "ricing" my window manager and I also don't
care how my desktop looks like. I have no wallpaper, it's just a black
background with a cyan bar in the top, I don't like beautiful and heavy
things, as my computer is not that powerful I prefer to save the much resources
as possible for the things I have to do.</p>

<h2>How I use the internet</h2>
<p>I am so strict on the way I use the internet.</p>
<p>I search things using lynx, actually I use lynx whenever is possible,
regretably there are some sites that makes doing that almost impossible, so
when I need a graphical browser I use tor.</p>
<p>I always have JavaScript disabled, unless it's strictly necessary I won't
turn it on. I also don't visit sites that uses Cloudflare, without exception,
if I open a website and it uses Cloudflare it won't let me in and I won't
make any change to visit that site.</p>
<p>I only use a "normal" browser (Icedove) with websites I trust. Actually,
I don't open any of my websites using that browser, I use tor to see my own
sites.</p>

<h2>Programming Languages</h2>
<p>I have used a lot of programming languages throughout my life, but I have
never taken one of them seriously, that's why I barely know something about all
of them.</p>
<p>Past year (<i>2020</i>) I started to take seriously a lot of things, such as
hacking and programming. I will only learn three languages: C, Perl and
NASM.</p>
<p>I use PHP a bit, I know how to do things there but the code sucks (this page
is written in PHP), I used to code a lot on JavaScript before but now I don't
like that language.</p>
<p>I've never liked Python or C++. I hate both of them.</p>

<h2>How I learned programming</h2>
<p>I discovered programming a long time ago (2013 or so). The first thing I
learned was to do little things in HTML and then I switched to GameMaker. Now I
feel like all the time I spent learning those "languages" and IDEs were a
complete waste of time. Neither HTML or GML are good programming languages, even
though thanks to them I discovered programming so at least it was not bad at
all.</p>
<p>Still I remember that <a
href="https://yewtu.be/watch?v=yJcoqOBklK4">this</a> was the first
programming video I ever saw in my life, I downloaded DreamWeaver and started to
do the things that guy was doing in that video.</p>
<p>Then I discovered <a
href="https://yewtu.be/channel/UCLXRGxAzeaLDGaOphqapzmg">this
channel</a> and started to learn CSS (My CSS sucks, I can't do anything with it,
I didn't learn anything haha)</p>
<p>And then, I discovered <a
href="https://yewtu.be/channel/UCPyrFm1DOHoTwDPv5kQqARg">this other channel</a>
and now I started learning GameMaker.</p>
<p>I kept using Game Maker and some other languages (PHP, HTML, CSS, JavaScript,
SQL) until I discovered Linux (my first Linux installation was Debian in August
26th, 2019), from there I discovered and started liking some other languages
like C, Perl and Assembly. Past year (2020) in December I decided to take things
serious and learn C, then I will learn Perl and then I will go for Assembly.</p>

<h2>Non-free software issues</h2>
<p>I won't run any non-free software on my computer, NO EXCEPTIONS. I hate
non-free software.</p>
<p>I am so strict with the things that are run on my computer, I won't accept
any non-free thing, I always try to convert the audio and video formats on my
computer, I run LibreJS (on Abrowser) and I will not enter in a website that
requires DRM it will NEVER be activated in my browser.</p>

<h2>Miscellaneous</h2>
<p>I will think about what to put on this section and I will update this.</p>
