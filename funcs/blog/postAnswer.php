<?php

function Blog_postAnswer($conn, $content, $answerTo) {
  date_default_timezone_set("America/Bogota");
  $date = date("Y-m-d H:i:s") . " (GMT-5)";

  $success = 0;

  $content = htmlspecialchars($content);

  if (!empty($content)) {
    $stmt = $conn->prepare("INSERT INTO blog (content, date, isAnswer, answerTo)
      VALUES(?,?,1,?)");
    $stmt->bind_param("ssi", $content, $date, $answerTo);
    $success = $stmt->execute();
    $stmt->close();
  }

  return $success;
}

?>
