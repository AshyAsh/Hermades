<?php

function Forum_getCommunities($conn) {
        $communities = [];
        
        $stmt = $conn->prepare("SELECT * FROM forums_communities ORDER BY id
                DESC");
        $stmt->execute();
        $stmt->bind_result($id, $name, $slug, $posts);
        while ($stmt->fetch()) {
                $communities[] = $id;
                $communities[] = $name;
                $communities[] = $slug;
                $communities[] = $posts;
        }
        $stmt->close();

        return $communities;
}

?>
