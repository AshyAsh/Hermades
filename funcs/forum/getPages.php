<?php

function Forum_getPages($conn, $community, $maxPerPage) {
  $stmt = $conn->prepare("SELECT id FROM forums_posts WHERE community=?");
  $stmt->bind_param("s", $community);
  $stmt->execute();
  $stmt->store_result();
  $total = $stmt->num_rows;
  $stmt->close();

  $pages = ceil($total/$maxPerPage);
  return $pages;
}

?>
