<?php

require "../inc/db.php";
include "../inc/head.php";

?>

<h1>Projects</h1>

<ul>
        <li><b><a href="/">Hermades</a></b>: A completely free (as in free-spech
and free beer) website. It contains (so far) a blog where I can post all the
things I am up to, and the things I think (it could be some kind of one-person
social network <3), a forum with several different "communities" where you can
post and comment in a completely freedom and privacy respecting way.</li>
        <li><b><a href="http://hschool.ml">Hermades' School</a></b>: A completely free
virtual school ran by me and contributors.</li>
</ul>
