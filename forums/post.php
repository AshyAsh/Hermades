<?php

require "../inc/db.php";
include "../inc/head.php";
include "../inc/libs/Parsedown.php";

include "../funcs/forum/getPost.php";
include "../funcs/forum/getAnswers.php";

if (empty($_GET["id"])) {
  die("<br>The entered post is not valid");
}

$id = $_GET["id"];

$post = Forum_getPost($conn, $id);
$community = $post[2];

if (isset($_POST["post"])) {
        include "../funcs/forum/addPost.php";

        date_default_timezone_set("UTC");

        $content = $_POST["postContent"];
        $date = date("d/m/Y H:i") . " UTC";

        $isAnswer = 0;
        $answerId = -1;

        $isAnswer = 1;
        $answerId = $id;

        if (Forum_addPost($conn, $community, $content, $date, $isAnswer,
          $answerId)) {
                echo "<br>Post added successfully";
                header("Reload: 0");
        }
}

$parsedown = new Parsedown();

?>
<hr>
<div class="post">
  <p><?php echo $parsedown->text($post[0]); ?></p>
  <p><b>Published on</b>: <?php echo $post[1]; ?></p>
  
  <div class="answers" style="border: 1px solid #f0a;">

    <form method="POST" style="padding: 5px;">
      <textarea cols="80" rows="1" placeholder="Answer to this post"
  name="postContent"></textarea>
      <input type="submit" value="Answer" name="post">
    </form>

      <div class="answer" style="padding: 5px;">
        <?php
          $answers = Forum_getAnswers($conn, $id, 100);

          for ($i = 0; $i < sizeof($answers); $i += 2) {
        ?>

          <p><?php echo $parsedown->text($answers[$i]); ?></p>
          <p><b>Published on</b>: <?php echo $answers[$i+1]; ?></p>

        <?php } ?>
      </div>

  </div>
</div>
