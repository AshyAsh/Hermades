<?php

include "../inc/head.php";

?>

<h1>Software that I use</h1>
<p>The following is a list of the FREE programs that I use:</p>

<ul>
	<li><b>Operating System</b>: I usually switch between Parabola and
	Trisquel, I've been using both of them for a long time now and they
	are so comfortable for me.</li>
	<li><b>Desktop Environment:</b> I don't use a desktop environment, I
	use a very lightweight window manager called dwm. I usually switch
	between dwm, i3, fluxbox and openbox.</li>
	<li><b>Browser:</b> I mainly use lynx, when I need to do something that
	requires a graphical browser I normally use Tor, and sometimes I use
	Abrowser for certain webpages only.</li>
	<li><b>File Manager:</b> I use ranger as file manager</li>
	<li><b>Programming IDE:</b> I don't use an IDE, I like text editors,
	I used vim for a long time but now I am using Emacs.</li>
	<li><b>Mail client:</b> I use IceDove as mail client.</li>
	<li><b>Video player:</b> Sometimes, when I need to play videos
	locally I use mpv, but sometimes when the subtitles are not
	supported by mpv I play the video on vlc (even though I hate it
	because its playback is so choppy).</li>
	<li><b>Social network:</b> I don't like social networks, even though
	I have some, I usually creates them because I want to have friends,
	we will talk a lot and it never happens, I have a mastodon account but
	it's been a long time since I don't make any update.</li>
	<li><b>Entertainment:</b> I do a lot of things when I am bored, I play
	the Emacs games, I read books, I enrol in courses, I watch videos using
	an invidious instance and things like that.</li>
</ul>
