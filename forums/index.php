<?php

require "../inc/db.php";
include "../inc/head.php";

?>

<h1>What is this?</h1>
<p>This is a completely freedom and privacy respecting forums place. Here you
can talk with people about common interests, hobbies, ask questions, talk about
you and several other things.</p>
<p>Every kind of illegal/nsfw content will be deleted and we are not responsible
for the posts made here</p>
<p><b>Note</b>: Entering HTML is disabled, you can use Markdown to include
media.</p>
<hr>
<h2>Available communities</h2>

<?php

include "../funcs/forum/getCommunities.php";

$communities = Forum_getCommunities($conn);

for ($i = 0; $i < sizeof($communities); $i += 4) {
?>

<p><a href="/forum/<?php echo $communities[$i+2]; ?>"><?php echo
$communities[$i+1]; ?></a> | <?php echo $communities[$i+3]; ?> posts</p>

<?php
}
?>
