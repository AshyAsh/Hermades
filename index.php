<?php

include "inc/head.php";

?>

<p>Hello, I am hermades!</p>

<h1>About</h1>

<p>Hi! this is my personal page. Here you can find things that I consider
interesting, there are forums, there is a blog, is a school and I am working on
several other things.</p>

<h3>Why?</h3>

<p>I created this site because I am tired of websites and companies that violate
your freedom and your privacy, that tracks you and that charge money for almost
eveverything.</p>
<p>You can visit and post in the <a href="/forums">forums</a>, you don't need to
sign in or to give personal information, you can post in a completely anonymous
way (even tho I don't endorse nor encourage any illegal activity, and those kind
of posts will be deleted)</p>
<p>You can also see our <a href="http://hschool.ml">school</a>, that's a place where I
will create, post and maintain courses (mainly about programming and hacking),
if you clone the source code of this website you can contribute with courses and
lessons (you could even create your very own course).</p>
<p>You can see the projects that I am currently working in <a
href="/projects">here</a>.</p>

<h3>About me</h3>

<p>You can find more information about me <a href="/about">here</a></p>
