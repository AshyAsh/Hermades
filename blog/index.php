<?php

require "../inc/db.php";
include "../inc/head.php";
include "../inc/libs/Parsedown.php";

$numPerPage = 20;
$page = 1;

if (isset($_GET["page"])) {
  $page = $_GET["page"];
}

$startFrom = ($page-1) * $numPerPage;

if (isset($_POST["ans"])) {
  include "../funcs/blog/postAnswer.php";

  if (Blog_postAnswer($conn, $_POST["content"], $_POST["id"])) {
    echo "Answer posted successfully";
  }
}

$parsedown = new Parsedown();

?>

<p>Welcome to my personal blog. This is a personal place, here I'll probably
post about things I am up to, things I learned, what I am feeling, what I am
thinking, etcetera.</p>

<?php

include "../funcs/blog/getPosts.php";
include "../funcs/blog/getAnswers.php";

$posts = Blog_getPosts($conn, $startFrom, $numPerPage);
for ($i = 0; $i < sizeof($posts); $i += 3) {
?>

<hr>
<p><?php echo $parsedown->text($posts[$i+1]); ?></p>
<p><b>Published on</b>: <?php echo $posts[$i+2]; ?></p>
<a href="/entry/<?php echo $posts[$i]; ?>">Full screen</a>
<div class="answers" style="border: 1px solid #f0a;">

  <?php
    $answers = Blog_getAnswers($conn, $posts[$i], 3);

    for ($j = 0; $j < sizeof($answers); $j += 2) {
  ?>

    <div class="answer" style="margin-left: 20px;">
      <p><?php echo $parsedown->text($answers[$j]); ?></p>
      <p><b>Published on</b>: <?php echo $answers[$j+1]; ?></p>
    </div>
    <hr>

  <?php } ?>

  <form class="answer" style="border: 1px solid #000; width: 100%; padding:
5px;" method="POST">
    <textarea rows="1" cols="80" name="content"></textarea>
    <input type="submit" name="ans" value="Answer">
    <input type="hidden" name="id" value="<?php echo $posts[$i]; ?>">
  </form>
</div>

<?php
  // Shall a few answers be displayed here?
}
?>

<?php

include "../funcs/blog/getPages.php";

$pages = Blog_getPages($conn, $numPerPage);

for ($i = 0; $i < $pages; $i++) {
  $page = $i+1;
  echo "<a href=\"?page=".$page."\">".$page."</a> ";
}

?>
