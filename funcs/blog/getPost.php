<?php

function Blog_getPost($conn, $pId) {
  $stmt = $conn->prepare("SELECT id,content,date FROM blog WHERE id=?");
  $stmt->bind_param("i", $pId);
  $stmt->execute();
  $stmt->bind_result($id, $content, $date);
  $stmt->fetch();
  $stmt->close();

  return [$id, $content, $date];
}

?>
