<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <title>Hermades</title>

        <link href="/css/style.css" rel="stylesheet">
</head>
<body>

<h1>Hermades personal site</h1>

<a class="link" href="/">Home</a>
<a class="link" href="/projects">Projects</a>
<a class="link" href="/about">About me</a>
<a class="link" href="/blog">Personal blog</a>
<a class="link" href="http://hschool.ml">School</a>
<a class="link" href="/forums">Forums</a>
<a class="link" href="/philosophy.php">Philosophy</a>
<a class="link" href="/donate.php">Donate</a>
<a class="link" href="https://codeberg.org/AshyAsh/Hermades">Source code</a>
