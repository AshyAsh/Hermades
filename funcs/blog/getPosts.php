<?php

function Blog_getPosts($conn, $page, $maxPerPage) {
        $posts = [];

        $stmt = $conn->prepare("SELECT id,content,date FROM blog WHERE
          isAnswer=0 ORDER BY id DESC LIMIT ?,?");
        $stmt->bind_param("ii", $page, $maxPerPage);
        $stmt->execute();
        $stmt->bind_result($id, $content, $date);
        while ($stmt->fetch()) {
                $posts[] = $id;
                $posts[] = $content;
                $posts[] = $date;
        }
        $stmt->close();

        return $posts;
}

?>
