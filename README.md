# Hermades

My personal Website. You can visit it [here](http://hermades.ml).

## What is it?

It's a completely freedom and privacy respecting website. So far there is only a
forum, a blog and a school (basically a place where I create courses).

**Note**: The code sucks. I am not good at PHP, I started this project so I
could improve it, so... Please say nice things <3

You can download the database file [here](http://hermades.ml)
