<?php

function Forum_getAnswers($conn, $postId, $limit) {
  $posts = [];

  $stmt = $conn->prepare("SELECT content,date FROM forums_posts WHERE
    isAnswer=1 AND answeredPost=? ORDER BY id DESC");
  $stmt->bind_param("i", $postId);
  $stmt->execute();
  $stmt->bind_result($content, $date);
  while ($stmt->fetch()) {
    $posts[] = $content;
    $posts[] = $date;
  }
  $stmt->close();

  return $posts;
}

?>
