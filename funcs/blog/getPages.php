<?php

function Blog_getPages($conn, $maxPerPage) {
  $stmt = $conn->prepare("SELECT id FROM blog WHERE isAnswer=0");
  $stmt->execute();
  $stmt->store_result();
  $total = $stmt->num_rows();
  $stmt->close();

  $pages = ceil($total/$maxPerPage);
  return $pages;
}

?>
